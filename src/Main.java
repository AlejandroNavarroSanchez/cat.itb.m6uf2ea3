import java.io.*;
import java.sql.*;
import java.util.HashMap;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws SQLException {
        Scanner scan = new Scanner(System.in).useLocale(Locale.US);
        int depId, dep_no, emp_no, dir;
        String depName, depLoc, cognom, ofici, fecha_alta;
        double salario, comision;

        Database db = Database.getInstance();
        db.connect();

//        ex1();
//        ex2();
//        ex3();
//        ex4();
//        ex5();
//        ex6();
//        ex7();

//        depId = scan.nextInt();
//        depName = scan.next();
//        depLoc = scan.next();
//        ex8(depId, depName, depLoc);

//        emp_no = scan.nextInt();
//        cognom = scan.next();
//        ofici = scan.next();
//        dir = scan.nextInt();
//        fecha_alta = scan.next();
//        salario = scan.nextDouble();
//        comision = scan.nextDouble();
//        dep_no = scan.nextInt();
//        ex9(emp_no, cognom, ofici, dir  , fecha_alta, salario, comision, dep_no);

//        emp_no = scan.nextInt();
//        salario = scan.nextDouble();
//        ex10(emp_no, salario);

//        ex11();
//        ex12();
//        ex13();
//        ex14();
//        ex15();
//        ex16();

        db.close();
    }
    public static void ex1() {
        File scriptFile = new File("HR.sql");
        System.out.println("\n\nFichero de consulta : " + scriptFile.getName());
        System.out.println("Convirtiendo el fichero a cadena...");
        BufferedReader entrada = null;
        try {
            entrada = new BufferedReader(new FileReader(scriptFile));
        } catch (FileNotFoundException e) {
            System.out.println("ERROR NO HAY FILE: " + e.getMessage());
        }
        String linea = null;
        StringBuilder stringBuilder = new StringBuilder();
        String salto = System.getProperty("line.separator");
        try {
            while ((linea = entrada.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(salto);
            }
        } catch (IOException e) {
            System.out.println("ERROR de E/S, al operar " + e.getMessage());
        }
        String consulta = stringBuilder.toString();
        System.out.println(consulta);

        try {
            Connection connmysql = Database.getInstance().getConnection();
            Statement sents = connmysql.createStatement();
            int res = sents.executeUpdate(consulta);
            System.out.println("Script creado con éxito, res = " + res);
            connmysql.close();
            sents.close();
        } catch (SQLException e) {
            System.out.println("ERROR AL EJECUTAR EL SCRIPT: " + e.getMessage());
        }
    }

    public static void ex2() {
        try {
            //Establecemos la conexion con la BD
            Connection conexion = Database.getInstance().getConnection();

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            String nombre  = dbmd.getDatabaseProductName();
            String driver  = dbmd.getDriverName();
            String url     = dbmd.getURL();
            String usuario = dbmd.getUserName() ;

            String[] tables = {"departamentos", "empleados"};

            System.out.println("INFORMACIÓN SOBRE LA BASE DE DATOS:");
            System.out.println("===================================");
            System.out.printf("Nombre : %s %n", nombre );
            System.out.printf("Driver : %s %n", driver );
            System.out.printf("URL    : %s %n", url );
            System.out.printf("Usuario: %s %n", usuario );

            for (String t : tables) {
                //Obtener informaci�n de las tablas y vistas que hay
                resul = dbmd.getTables(null, null, t.toLowerCase(Locale.ROOT), null);

                while (resul.next()) {
                    System.out.println();
                    String catalogo = resul.getString(1);//columna 1
                    String esquema = resul.getString(2); //columna 2
                    String tabla = resul.getString(3);   //columna 3
                    String tipo = resul.getString(4);	  //columna 4
                    System.out.printf("%s - Catalogo: %s, Esquema: %s, Nombre: %s %n",
                            tipo, catalogo, esquema, tabla);
                }
            }

        } catch (SQLException e) {e.printStackTrace();}
    }

    public static void ex3() {
        try
        {
            //Establecemos la conexion con la BD
            Connection conexion = Database.getInstance().getConnection();

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("COLUMNAS TABLA EMPLEADOS:");
            System.out.println("===================================");
            ResultSet columnas=null;
            columnas = dbmd.getColumns(null, null, "empleados", null);
            while (columnas.next()) {
                String nombCol = columnas.getString("COLUMN_NAME"); //getString(4)
                String tipoCol = columnas.getString("TYPE_NAME");   //getString(6)
                String tamCol = columnas.getString("COLUMN_SIZE");  //getString(7)
                String nula  = columnas.getString("IS_NULLABLE");   //getString(18)

                System.out.printf("Columna: %s, Tipo: %s, Tamaño: %s, ¿Puede ser Nula?: %s %n", nombCol, tipoCol, tamCol, nula);
            }

        } catch (SQLException e) {e.printStackTrace();}
    }

    public static void ex4() {
        try
        {
            //Establecemos la conexion con la BD
            Connection conexion = Database.getInstance().getConnection();

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            System.out.println("COLUMNAS TABLA DEPARTAMENTOS:");
            System.out.println("===================================");
            ResultSet columnas=null;
            columnas = dbmd.getColumns(null, null, "departamentos", null);
            while (columnas.next()) {
                String nombCol = columnas.getString("COLUMN_NAME"); //getString(4)
                String tipoCol = columnas.getString("TYPE_NAME");   //getString(6)
                String tamCol = columnas.getString("COLUMN_SIZE");  //getString(7)
                String nula  = columnas.getString("IS_NULLABLE");   //getString(18)

                System.out.printf("Columna: %s, Tipo: %s, Tamaño: %s, ¿Puede ser Nula?: %s %n", nombCol, tipoCol, tamCol, nula);
            }

        } catch (SQLException e) {e.printStackTrace();}
    }

    public static void ex5() {
        try {
            //Establecemos la conexion con la BD
            Connection conexion = Database.getInstance().getConnection();

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            String[] tables = {"departamentos", "empleados"};

            for (String t : tables) {
                System.out.println("CLAVE PRIMARIA TABLA " + t.toUpperCase(Locale.ROOT) + ":");
                System.out.println("===================================");
                ResultSet pk = dbmd.getPrimaryKeys(null, null, t.toLowerCase(Locale.ROOT));
                StringBuilder pkDep = new StringBuilder();
                String separador = "";
                while (pk.next()) {
                    pkDep.append(separador).append(pk.getString("COLUMN_NAME"));//getString(4)
                    separador = "+";
                }
                System.out.println("Clave Primaria: " + pkDep + "\n");
            }
            System.out.println();

            System.out.println("CLAVES ajenas que referencian a DEPARTAMENTOS:");
            System.out.println("==============================================");

            for (String t : tables) {
                ResultSet fk = dbmd.getExportedKeys(null, null, t.toLowerCase(Locale.ROOT));
                while (fk.next()) {
                    String fk_name = fk.getString("FKCOLUMN_NAME");
                    String pk_name = fk.getString("PKCOLUMN_NAME");
                    String pk_tablename = fk.getString("PKTABLE_NAME");
                    String fk_tablename = fk.getString("FKTABLE_NAME");
                    System.out.printf("Tabla PK: %s, Clave Primaria: %s %n", pk_tablename, pk_name);
                    System.out.printf("Tabla FK: %s, Clave Ajena: %s %n", fk_tablename, fk_name);
                }
            }

        } catch (SQLException e) {e.printStackTrace();}
    }

    public static void ex6() {
        try {
            Connection conexion = Database.getInstance().getConnection();
            String sql = "SELECT * FROM EMPLEADOS";
            Statement sentencia = conexion.createStatement();
            boolean valor = sentencia.execute(sql);

            System.out.println("TABLA EMPLEADOS");
            System.out.println("===================================");
            if (valor) {
                ResultSet rs = sentencia.getResultSet();
                while (rs.next())
                    System.out.printf("[COD: %d], [%s], [%s], [%d], [%s], [%.2f], [%.2f], [%d] %n",
                            rs.getInt(1), rs.getString(2), rs.getString(3),
                            rs.getInt(4), rs.getString(5), rs.getDouble(6),
                            rs.getDouble(7), rs.getInt(8));
                rs.close();
            } else {
                int f = sentencia.getUpdateCount();
                System.out.printf("Filas afectadas:%d %n", f);
            }

            sentencia.close();
            conexion.close();
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
    }

    public static void ex7() {
        try {
            Connection conexion = Database.getInstance().getConnection();
            String sql = "SELECT * FROM DEPARTAMENTOS";
            Statement sentencia = conexion.createStatement();
            boolean valor = sentencia.execute(sql);

            System.out.println("TABLA DEPARTAMENTOS");
            System.out.println("===================================");
            if (valor) {
                ResultSet rs = sentencia.getResultSet();
                while (rs.next())
                    System.out.printf("[COD: %d], [%s], [%s] %n",
                            rs.getInt(1), rs.getString(2), rs.getString(3));
                rs.close();
            } else {
                int f = sentencia.getUpdateCount();
                System.out.printf("Filas afectadas:%d %n", f);
            }

            sentencia.close();
            conexion.close();
        } catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
    }

    public static void ex8(int id, String name, String loc) {
        try {
            // Establecemos la conexion con la BD
            Connection conexion = Database.getInstance().getConnection();

            // construir orden INSERT
            String sql = "INSERT INTO departamentos VALUES ( ?, ?, ? )";

            System.out.println(sql);
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, id);
            sentencia.setString(2, name.toUpperCase(Locale.ROOT));
            sentencia.setString(3, loc.toUpperCase(Locale.ROOT));

            int filas;//
            try {
                filas = sentencia.executeUpdate();
                System.out.println("Filas afectadas: " + filas);
            } catch (SQLException e) {
                System.out.println("HA OCURRIDO UNA EXCEPCIÓN:");
                System.out.println("Mensaje:    "+ e.getMessage());
                System.out.println("SQL estado: "+ e.getSQLState());
                System.out.println("COD error:  "+ e.getErrorCode());
            }
            sentencia.close(); // Cerrar Statement

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void ex9(int emp_no, String cognom, String ofici, int cap, String data_alta, double salari, double comissio, int dep_no) {
        try {
            Connection connection = Database.getInstance().getConnection();
            String query = "INSERT INTO empleados values (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);

            insertStatement.setInt(1, emp_no);
            insertStatement.setString(2, cognom);
            insertStatement.setString(3, ofici);
            insertStatement.setInt(4, cap);
            insertStatement.setDate(5, Date.valueOf(data_alta)); // Transforms a String into java.sql.Date
            insertStatement.setDouble(6, salari);
            insertStatement.setDouble(7, comissio);
            insertStatement.setInt(8, dep_no);
            insertStatement.executeUpdate();
            System.out.println("Empleat insertat correctament");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void ex10(int emp_no, double salario) {
        try {
            Connection connection = Database.getInstance().getConnection();
            String query = "UPDATE empleados SET salario = salario + ? WHERE emp_no = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);

            HashMap<Integer, Double> codsPlusLimits = new HashMap<>();
            codsPlusLimits.put(emp_no, salario);

            codsPlusLimits.forEach((key, value) -> {
                try {
                    insertStatement.setDouble(1, value);
                    insertStatement.setInt(2, key);
                    insertStatement.executeUpdate();
                    System.out.println("Salari de l'empleat amb codi " + key + " actualitzat correctament a " + value);
                } catch (SQLException sqle) {
                    sqle.getMessage();
                }
            });

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void ex11() throws SQLException {
        // Establecemos la conexion con la BD
        Connection conexion = Database.getInstance().getConnection();

        StringBuilder sql = new StringBuilder();
        sql.append("CREATE OR REPLACE VIEW totales ");
        sql.append("(dep, dnombre, nemp, media) AS ");
        sql.append("SELECT d.dept_no, dnombre, COUNT(emp_no), AVG(salario) ");
        sql.append("FROM departamentos d LEFT JOIN empleados e " );
        sql.append("ON e.dept_no = d.dept_no ");
        sql.append("GROUP BY d.dept_no, dnombre ");
        System.out.println(sql);

        Statement sentencia = conexion.createStatement();
        int filas = sentencia.executeUpdate(sql.toString());
        System.out.printf("Resultado  de la ejecución: %d %n", filas);

        sentencia.close(); // Cerrar Statement

        // Select vista
        String sql2 = "SELECT * FROM totales";
        PreparedStatement st = conexion.prepareStatement(sql2);

        ResultSet rs = st.executeQuery();
        while (rs.next())
            System.out.printf("dep: %d, dnombre: %s, count_emp: %d, media_salario: %.2f %n", rs.getInt(1),
                    rs.getString(2), rs.getInt(3), rs.getDouble(4));

        rs.close();// liberar recursos
        st.close();
    }

    public static void ex12() throws SQLException {
        // Establecemos la conexion con la BD
        Connection conexion = Database.getInstance().getConnection();

        StringBuilder sql = new StringBuilder();
        sql.append("CREATE PROCEDURE subida_sal (d INT, subida INT) AS $$");
        sql.append("BEGIN ");
        sql.append("UPDATE empleados SET salario = salario + subida WHERE dept_no = d; ");
        sql.append("COMMIT; " );
        sql.append("END; ");
        sql.append("$$ language plpgsql");

        System.out.println(sql);

        Statement sentencia = conexion.createStatement();
        int filas = sentencia.executeUpdate(sql.toString());
        System.out.printf("Resultado  de la ejecución: %d %n", filas);

        sentencia.close(); // Cerrar Statement
    }

    public static void ex13() throws SQLException {
        // Establecemos la conexion con la BD
        Connection conexion = Database.getInstance().getConnection();

        String sql = " CREATE FUNCTION nombre_dep35(dep INT) RETURNS VARCHAR(15) AS $$ DECLARE nom VARCHAR(15) DEFAULT 'INEXISTENT'; BEGIN SELECT dnombre INTO nom FROM departamentos WHERE dept_no = dep; RETURN nom; END; $$ LANGUAGE plpgsql;";

        Statement sentencia = conexion.createStatement();
        int filas = sentencia.executeUpdate(sql);

        System.out.printf("Resultat de l'execució: %d %n", filas);

        sentencia.close(); // Cerrar Statement
    }

    public static void ex14() {
        try {
            Connection conexion = Database.getInstance().getConnection();
            DatabaseMetaData dbmd = conexion.getMetaData();// Creamos objeto
            // DatabaseMetaData

            ResultSet proc = dbmd.getProcedures(null, null, "subida_sal");
            while (proc.next()) {
                String proc_name = proc.getString("PROCEDURE_NAME");
                String proc_type = proc.getString("PROCEDURE_TYPE");
                System.out.printf("Nombre Procedimiento: %s - Tipo: %s %n", proc_name, proc_type);
            }

            ResultSet func = dbmd.getFunctions(null, null, "nombre_dep35");
            while (func.next()) {
                String function_name = func.getString("FUNCTION_NAME");
                String function_type = func.getString("FUNCTION_TYPE");
                System.out.printf("Nombre Procedimiento: %s - Tipo: %s %n", function_name, function_type);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void ex15() {
        try {

            Connection conexion = Database.getInstance().getConnection();

            String dep = "10";   //"10"; // departamento
            int subida = 1000;//"1000"; // subida

            // construir orden DE LLAMADA
            String sql = "call subida_sal (?, ?)";

            // Preparamos la llamada
            CallableStatement llamada = conexion.prepareCall(sql);
            // Damos valor a los argumentos
            llamada.setInt(1, Integer.parseInt(dep)); // primer argumento-dep
            llamada.setInt(2, subida); // segundo arg

            llamada.execute(); // ejecutar el procedimiento
            System.out.println("Subida realizada....");
            llamada.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void ex16() {
        try {

            Connection conexion = Database.getInstance().getConnection();

            String dep = "10";

            String sql = "{ ? = call nombre_dep35 (?) } "; // MYSQL

            // Preparamos la llamada
            CallableStatement llamada = conexion.prepareCall(sql);

            llamada.registerOutParameter(1, Types.VARCHAR); // valor devuelto
            llamada.setInt(2, Integer.parseInt(dep)); // param de entrada

            llamada.executeUpdate(); // ejecutar el procedimiento
            System.out.println("Nombre Dep: " + llamada.getString(1));
            llamada.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
