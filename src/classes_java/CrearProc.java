package classes_java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;

public class CrearProc {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");// Cargar el driver
		// Establecemos la conexion con la BD
		Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost/HR","user1", "pass1");

		StringBuilder sql = new StringBuilder();
        sql.append("CREATE PROCEDURE `subida_sal` (d INT, subida INT) ");
        sql.append("BEGIN ");
        sql.append("UPDATE empleados SET salario = salario + subida WHERE dept_no = d; ");
        sql.append("COMMIT; " );
        sql.append("END; ");

		System.out.println(sql);
		
		Statement sentencia = conexion.createStatement();
		int filas = sentencia.executeUpdate(sql.toString());
		System.out.printf("Resultado  de la ejecución: %d %n", filas);

		sentencia.close(); // Cerrar Statement
		conexion.close(); // Cerrar conexi�n

	}// fin de main
}// fin de la clase
