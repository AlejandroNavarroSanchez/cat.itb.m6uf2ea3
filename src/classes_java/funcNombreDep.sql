DELIMITER //
CREATE FUNCTION nombre_dep (d int)
RETURNS VARCHAR(15) DETERMINISTIC
BEGIN
DECLARE nom VARCHAR(15);
SET nom = 'INEXISTENTE';
SELECT dnombre INTO nom FROM departamentos WHERE dept_no=d;
RETURN nom;
END//
DELIMITER ;
