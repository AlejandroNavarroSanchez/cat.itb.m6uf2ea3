SET SCHEMA 'HR';
DROP procedure IF EXISTS subida_sal;

CREATE OR REPLACE FUNCTION subida_sal (d INT, subida INT) RETURNS VOID
AS $$
BEGIN
UPDATE empleados SET salario = salario + subida WHERE dept_no = d;
/* COMMIT; */
END;
$$ LANGUAGE plpgsql;
