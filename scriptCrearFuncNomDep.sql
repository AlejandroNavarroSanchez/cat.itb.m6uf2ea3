SET SCHEMA 'HR';
DROP function IF EXISTS nombre_dep;

CREATE OR REPLACE FUNCTION nombre_dep (d int) 
RETURNS VARCHAR(15)
AS $$

DECLARE nom VARCHAR(15);
BEGIN
nom := 'INEXISTENTE';

SELECT dnombre INTO nom FROM departamentos
WHERE dept_no=d;
RETURN nom;
END;
$$ LANGUAGE plpgsql;
